<div class="container">
	<div class="row my-4">
		<div class="col">
			<h2>MJP-REST-CLIENT-EXAMPLE</h2>
		</div>
	</div>

	<div class="row mb-2">
		<div class="col">
			<h3>CRUD - Product</h3>
		</div>
		<div class="col text-right">
			<button class="btn btn-success btn-sm" data-target="#modal-add" data-toggle="modal">TAMBAH</button>
		</div>
	</div>

	<div class="row">
		<div class="col">
			<div class="table-responsive">
				<table class="table table-hover">
					<thead>
						<tr>
							<th scope="col" width="260">Gambar</th>
							<th scope="col">Nama</th>
							<th scope="col">Harga</th>
							<th scope="col">Deskripsi</th>
							<th scope="col" width="20">Aksi</th>
						</tr>
					</thead>
					<tbody id="body-table">

					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>


<!-- Modal -->
<div class="modal fade" id="modal-add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Tambah Produk</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label for="">Nama Produk</label>
					<input type="text" name="" id="nama" class="form-control" placeholder="Masukkan nama produk ...">
				</div>

				<div class="form-group">
					<label for="">Harga Produk</label>
					<input type="number" name="" id="harga" class="form-control" placeholder="Masukkan harga produk ...">
				</div>

				<div class="form-group">
					<label for="">Deskripsi</label>
					<textarea name="" id="desc" cols="30" rows="5" class="form-control" placeholder="Masukkan deskripsi produk ..."></textarea>
				</div>

				<div class="form-group">
					<div class="input-group mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text" id="inputGroupFileAddon01">Upload</span>
						</div>
						<div class="custom-file">
							<input type="file" class="custom-file-input" id="gambar" aria-describedby="inputGroupFileAddon01" onchange="getBase64(this)">
							<label class="custom-file-label" for="inputGroupFile01">Pilih gambar produk ...</label>
						</div>
					</div>
				</div>

				<div class="form-group">
					<img src="" id="img" class="img-fluid img-responsive">
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary btn-sm" id="btn-add-product">Save changes</button>
			</div>
		</div>
	</div>
</div>


<!-- Modal -->
<div class="modal fade" id="modal-edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Edit Produk</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<input type="hidden" name="" id="edit-id" value="">
				<div class="form-group">
					<label for="">Nama Produk</label>
					<input type="text" name="" id="edit-nama" class="form-control" placeholder="Masukkan nama produk ...">
				</div>

				<div class="form-group">
					<label for="">Harga Produk</label>
					<input type="number" name="" id="edit-harga" class="form-control" placeholder="Masukkan harga produk ...">
				</div>

				<div class="form-group">
					<label for="">Deskripsi</label>
					<textarea name="" id="edit-desc" cols="30" rows="5" class="form-control" placeholder="Masukkan deskripsi produk ..."></textarea>
				</div>

				<div class="form-group">
					<div class="input-group mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text" id="edit-inputGroupFileAddon01">Upload</span>
						</div>
						<div class="custom-file">
							<input type="file" class="custom-file-input" id="edit-gambar" aria-describedby="inputGroupFileAddon01" onchange="getBase64(this)">
							<label class="custom-file-label" for="inputGroupFile01" id="edit-label-img">Pilih gambar produk ...</label>
						</div>
					</div>
				</div>

				<div class="form-group">
					<img src="" id="edit-img" class="img-fluid img-responsive">
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary btn-sm" id="btn-edit-product">Save changes</button>
			</div>
		</div>
	</div>
</div>


<script>
	let encodedImage, encodedImageEdit
	const APILINK = 'http://localhost/mjp-web-service-ci/'

	$.ajaxSetup({
		headers: {
			'x-Auth': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9leGFtcGxlLm9yZyIsImF1ZCI6Imh0dHA6XC9cL2V4YW1wbGUuY29tIiwiaWF0IjoxNTk1NjY4MjczLCJuYmYiOjE1OTU2NjgyODMsImRhdGEiOnsiaWQiOiI5IiwibmFtYSI6InNhaWQiLCJlbWFpbCI6InNhaWRAc2FpZC5jb20ifX0.spshyere5HXS0yNQupn9pEauV4hEBnR8goCaL6yYR94',
			'Content-Type': 'application/x-www-form-urlencoded'
		}
	});

	const getBase64 = (file) => {
		let reader = new FileReader();

		reader.onload = (e) => {
			encodedImage = e.target.result
			encodedImageEdit = e.target.result
			$('#img').attr('src', e.target.result)
			$('#edit-img').attr('src', e.target.result)
		}
		reader.readAsDataURL(file.files[0])
	}

	const NP = {
		start() {
			NProgress.start()
		},
		done() {
			NProgress.done()
		}
	}

	const SW = {
		show(opt) {
			return Swal.fire(opt)
		},
		toast(opt) {
			return SW.show({
				...opt,
				toast: true,
				position: 'top-right',
				timer: 2000,
				showConfirmButton: false
			})
		}
	}

	const clearField = () => {
		$('#nama').val('')
		$('#harga').val('')
		$('#desc').val('')
		encodedImage = ''
	}

	function formatRupiah(angka, prefix) {
		var number_string = angka.replace(/[^,\d]/g, '').toString(),
			split = number_string.split(','),
			sisa = split[0].length % 3,
			rupiah = split[0].substr(0, sisa),
			ribuan = split[0].substr(sisa).match(/\d{3}/gi);

		// tambahkan titik jika yang di input sudah menjadi angka ribuan
		if (ribuan) {
			separator = sisa ? '.' : '';
			rupiah += separator + ribuan.join('.');
		}

		rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
		return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
	}

	const getDataproduct = () => {
		$.ajax({
			method: 'GET',
			url: APILINK + 'product',
			beforeSend: () => {
				NP.start()
			},
			success: (res) => {
				let createElement = ''

				if (res.data.length > 0) {
					res.data.forEach((data, it) => {
						createElement += `
						<tr>
							<th scope="row">
								<img src="${APILINK + data.gambar}" alt="" class="img-thumbnail" width="250">
							</th>
							<td><b>${data.nama}</b></td>
							<td>${formatRupiah(data.harga,'Rp.')}</td>
							<td>${data.deskripsi}</td>
							<td>
								<div class="btn-group">
									<button class="btn btn-sm btn-danger" onClick="deleteDataProduct(${data.id})">
										<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-trash" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
											<path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z" />
											<path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4L4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z" />
										</svg>
									</button>
									<button class="btn btn-sm btn-warning text-white" onClick="editDataProduct(${data.id})"><svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-pencil" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
											<path fill-rule="evenodd" d="M11.293 1.293a1 1 0 0 1 1.414 0l2 2a1 1 0 0 1 0 1.414l-9 9a1 1 0 0 1-.39.242l-3 1a1 1 0 0 1-1.266-1.265l1-3a1 1 0 0 1 .242-.391l9-9zM12 2l2 2-9 9-3 1 1-3 9-9z" />
											<path fill-rule="evenodd" d="M12.146 6.354l-2.5-2.5.708-.708 2.5 2.5-.707.708zM3 10v.5a.5.5 0 0 0 .5.5H4v.5a.5.5 0 0 0 .5.5H5v.5a.5.5 0 0 0 .5.5H6v-1.5a.5.5 0 0 0-.5-.5H5v-.5a.5.5 0 0 0-.5-.5H3z" />
										</svg>
									</button>
								</div>
							</td>
						</tr>
					`
					});
				} else {
					createElement += `
						<tr>
							<td colspan="5">Tidak ada data yang ditemukan</td>
						</tr>
					`
				}

				$('#body-table').html(createElement)
				NP.done()
			}
		})
	}

	const deleteDataProduct = (id) => {
		SW.show({
			title: 'Peringatan !',
			text: 'Apakah anda ingin menghapus data tersebut .?',
			showCancelButton: true,
			icon: 'warning'
		}).then(res => {
			if (res.value) {
				$.ajax({
					url: APILINK + 'product/delete',
					method: 'DELETE',
					data: {
						id
					},
					beforeSend: () => {
						NP.start()
					},
					success: (res) => {
						SW.toast({
							title: res.pesan,
							icon: res.status == 1 ? 'success' : 'error'
						})
						getDataproduct()
						NP.done()
					}
				})
			}
		})
	}

	const editDataProduct = (id) => {
		$.ajax({
			method: 'GET',
			url: APILINK + 'product/' + id,
			beforeSend: () => {
				NP.start()
			},
			success: (res) => {
				if (res.status == 1) {
					console.log(res);
					$('#edit-nama').val(res.data.nama)
					$('#edit-harga').val(res.data.harga)
					$('#edit-desc').val(res.data.deskripsi)
					$('#edit-img').attr('src', APILINK + res.data.gambar)
					$('#edit-id').val(res.data.id)
					$('#modal-edit').modal('show')
				}
				NP.done()
			}
		})
	}

	$(() => {
		getDataproduct()
		$('#btn-add-product').click(() => {
			let nama, harga, deskripsi, gambar;

			nama = $('#nama').val()
			harga = $('#harga').val()
			deskripsi = $('#desc').val()
			gambar = encodedImage


			$.ajax({
				method: 'POST',
				url: APILINK + 'product/add',
				data: {
					nama,
					harga,
					deskripsi,
					gambar
				},
				beforeSend: () => {
					NP.start()
				},
				success: (res) => {

					if (res.status == 1) {
						SW.toast({
							title: res.pesan,
							icon: 'success'
						});
						$('#modal-add').modal('hide')
						clearField()
					} else {
						SW.toast({
							title: res.pesan,
							icon: 'error'
						})
					}
					NP.done()
					getDataproduct()
				}
			})

		})

		$('#btn-edit-product').click(() => {
			let nama, harga, deskripsi, gambar;

			nama = $('#edit-nama').val()
			harga = $('#edit-harga').val()
			deskripsi = $('#edit-desc').val()
			gambar = encodedImageEdit
			id = $('#edit-id').val()


			$.ajax({
				method: 'PUT',
				url: APILINK + 'product/edit',
				data: {
					nama,
					harga,
					deskripsi,
					gambar,
					id
				},
				beforeSend: () => {
					NP.start()
				},
				success: (res) => {

					if (res.status == 1) {
						SW.toast({
							title: res.pesan,
							icon: 'success'
						});
						$('#modal-edit').modal('hide')
						clearField()
					} else {
						SW.toast({
							title: res.pesan,
							icon: 'error'
						})
					}
					NP.done()
					getDataproduct()
				}
			})

		})

		$('#modal-add').on('show.bs.modal', () => {
			encodedImage = ''
			$('#img').attr('src', '')
		})


		$('#modal-edit').on('show.bs.modal', () => {
			encodedImageEdit = ''
		})
	})
</script>
