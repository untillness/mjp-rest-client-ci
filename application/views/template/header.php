<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>MJP-REST-CLIENT</title>

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
	<!-- <link rel="stylesheet" href="https://unpkg.com/nprogress@0.2.0/nprogress.css" crossorigin="anonymous"> -->
	<style>
		/* Make clicks pass-through */
		#nprogress {
			pointer-events: none;
		}

		#nprogress .bar {
			background: #29d;

			position: fixed;
			z-index: 99999;
			top: 0;
			left: 0;

			width: 100%;
			height: 5px;
		}

		/* Fancy blur effect */
		#nprogress .peg {
			display: block;
			position: absolute;
			right: 0px;
			width: 100px;
			height: 100%;
			box-shadow: 0 0 10px #29d, 0 0 5px #29d;
			opacity: 1.0;

			-webkit-transform: rotate(3deg) translate(0px, -4px);
			-ms-transform: rotate(3deg) translate(0px, -4px);
			transform: rotate(3deg) translate(0px, -4px);
		}

		/* Remove these to get rid of the spinner */
		#nprogress .spinner {
			display: none;
			position: fixed;
			z-index: 1031;
			top: 15px;
			right: 15px;
		}

		#nprogress .spinner-icon {
			width: 18px;
			height: 18px;
			box-sizing: border-box;

			border: solid 2px transparent;
			border-top-color: #29d;
			border-left-color: #29d;
			border-radius: 50%;

			-webkit-animation: nprogress-spinner 400ms linear infinite;
			animation: nprogress-spinner 400ms linear infinite;
		}

		.nprogress-custom-parent {
			overflow: hidden;
			position: relative;
		}

		.nprogress-custom-parent #nprogress .spinner,
		.nprogress-custom-parent #nprogress .bar {
			position: absolute;
		}

		@-webkit-keyframes nprogress-spinner {
			0% {
				-webkit-transform: rotate(0deg);
			}

			100% {
				-webkit-transform: rotate(360deg);
			}
		}

		@keyframes nprogress-spinner {
			0% {
				transform: rotate(0deg);
			}

			100% {
				transform: rotate(360deg);
			}
		}
	</style>

	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
	<script src="https://unpkg.com/nprogress@0.2.0/nprogress.js"></script>

</head>


<body>
